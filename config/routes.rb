Rails.application.routes.draw do
  root 'home#index'
  get 'scrape_data', action: :scrape_data, controller: 'home'
  resources :home
end
