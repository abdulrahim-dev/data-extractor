# frozen_string_literal: true

require 'test_helper'

class UrlServiceTester < ActiveSupport::TestCase
  test('valid_url? validate true url') do
    assert UrlService.valid_url?('https://uae.yallamotor.com/')
  end

  test 'valid_url? validate false url' do
    assert_not UrlService.valid_url?('invalid user input')
  end
end
