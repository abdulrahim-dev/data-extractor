require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get home_index_url
    assert_response :success
  end

  test 'should get scrape_data' do
    get scrape_data_url url: 'https://uae.yallamotor.com/new-cars/toyota'
    assert_response :success
  end
end
