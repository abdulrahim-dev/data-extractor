# frozen_string_literal: true

require 'open-uri'

class ScrapeService
  MODEL_BLOCK_CSS = 'div.list-models'
  MODEL_NAME_CSS = '.pro-name'
  MODEL_PRICE_CSS = '.pro-pice'
  MODEL_DETAIL_BTN = '.btns-block a'
  MODEL_VERSION_BLOCK_CSS = 'div.version-head'
  CSV_HEADERS = %w[TYPE TITLE PRICE].freeze
  SITE_URL_REGEX = '^(http(s)?:\/\/)?((w){3}.)?(uae|ksa|egypt|qatar|oman|kuwait|bahrain).?yallamotor?(\.com)?\/new-cars\/(.*)'

  # @param [String] site_url
  def initialize(site_url)
    @site_url = site_url
    @base_url = site_url.split('.com').first.concat('.com')
  end

  def cars_list
    base_doc = get_parsed_doc @site_url
    return nil unless base_doc

    futures = get_page_models_list(base_doc).map do |car_model|
      Concurrent::Future.execute { get_car_versions(car_model) }
    end
    futures.collect_concat(&:value)
  end

  private

  def get_parsed_doc(page_url)
    raw_page = get_page(page_url)
    Nokogiri::HTML(raw_page)
  end

  def get_page(page_url)
    begin
      URI.open(page_url)
    rescue OpenURI::HTTPError => e
      return nil unless e.io.status.first == '429'

      get_page(page_url) # Retry if 429 (Too many requests) occurs
    end
  end

  def get_car_versions(car)
    car_version_url = @base_url + car[:pro_detail_url]
    car_detail_doc = get_parsed_doc(car_version_url)
    car_versions_list = scrape_car_versions_list(car_detail_doc)
    car_model = [{car_model: car[:pro_name]}]
    car_model.concat(car_versions_list)
  end

  # @param [Nokogiri::HTML::Document] car_detail_doc
  def scrape_car_versions_list(car_detail_doc)
    car_detail_doc.search(MODEL_VERSION_BLOCK_CSS).map(&method(:map_car_version_detail))
  end

  # @param [Nokogiri::HTML::Node] car_versions_node
  def map_car_version_detail(car_versions_node)
    {
        version_name: car_versions_node.children.at_css(MODEL_NAME_CSS).text.strip,
        version_price: car_versions_node.children.at_css(MODEL_PRICE_CSS).text.strip
    }
  end

  # @param [Nokogiri::HTML::Document] doc
  def get_page_models_list(doc)
    get_all_models(doc).select { |m| m[:pro_detail_url].include? 'new-car' }
  end

  # @param [Nokogiri::HTML::Document] doc
  def get_all_models(doc)
    doc.search(MODEL_BLOCK_CSS).map(&method(:map_car_model_details))
  end

  # @param [Nokogiri::HTML::Node] model
  def map_car_model_details(model)
    {
      pro_name: model.children.css(MODEL_NAME_CSS).text.strip,
      pro_detail_url: model.children.css(MODEL_DETAIL_BTN).first['href']
    }
  end
end
