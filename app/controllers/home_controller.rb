# frozen_string_literal: true

class HomeController < ApplicationController

  def index
  end

  def scrape_data
    url = params[:url]
    return invalid_url unless UrlService.valid_url?(url)

    return not_supported_url unless url.match(ScrapeService::SITE_URL_REGEX)

    scrape_service = ScrapeService.new(url)
    cars = scrape_service.cars_list
    return no_cars_found unless cars.any?

    @message_type = 'success'
    @messages = ["#{cars.count} cars extracted"]
    @cars_table = cars.map(&method(:cars_table_format))
  end

  private

  def cars_table_format(item)
    if item.key? :car_model
      {type: 'Car Model', title: item[:car_model], price: nil}
    elsif item.key? :version_name
      {
        type: 'Car Version',
        title: item[:version_name],
        price: item[:version_price]
      }
    end
  end

  def invalid_url
    @message_type = 'danger'
    @messages = ['Invalid URL']
    render :index
  end

  def not_supported_url
    @message_type = 'danger'
    @messages = ['Provided yallamotor.com URL is not supported']
    render :index
  end

  def no_cars_found
    @message_type = 'danger'
    @messages = ['No car found']
    render :index
  end
end

