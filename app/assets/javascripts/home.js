$(document).ready(function() {
    var table = $("#car-versions").DataTable({
        ordering: false,
        buttons: {
            buttons: [
                { extend: 'csv', text: 'Export as CSV' },
                { extend: 'pdf', text: 'Export as PDF' },
                { extend: 'print', text: 'Print' },
            ]
        }
    });
    table.buttons().container()
        .appendTo('#car-versions_wrapper .col-md-6:eq(0)');

    $('#data-form').submit(function () {
        $('#submit_spinner').removeClass('hidden').addClass('show');
    });
});
