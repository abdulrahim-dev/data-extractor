# [Data Extractor](https://data-extractor.herokuapp.com)
 A simple Cars data extractor that scrape from [YallaMotors.](https://www.yallamotor.com/)

## Table of contents
 * [General info](#general-info)
 * [Technologies](#technologies)
 * [Setup](#setup)
 * [Examples](#examples)

## General info
  Data Extractor scrape cars models & versions from [yallamotors.com](https://www.yallamotor.com/)
  currently it only support new-cars pages to extract the information.The scraped data is than displayed in table that could be exported in CSV & PDF format.
  
  
## Technologies

 * Ruby 2.6.0
 * Rails 5.2.3
 * concurrent-ruby
 * jquery-datatables
 * Bootstrap 4
 
## Setup

First, install the gems required by the application:

    bundle install
 Next, run application

	bin/rails serve

## Examples
 Provide url https://ksa.yallamotor.com/new-cars/chevrolet in form
 ![form-screenshot](https://drive.google.com/uc?export=view&id=1z6LdVhNPBrwGIPzZInEFK1bGldsGfeBu)
 
 On Submit we get scraped date in a table
 ![data-table](https://drive.google.com/uc?export=view&id=12bvqtHQd4bO6zQEoLeASf5vaNyz7gjWT)
    
  That could be exported in CSV format
  ![data-csv](https://drive.google.com/uc?export=view&id=1waEyG45-pATRnnHXHmq-Bp6whCbIjhiI)

  And in PDF format
	![data-pdf](https://drive.google.com/uc?export=view&id=1huDPhDdlWCUvx2djLLPX806cAaYhj18K)
